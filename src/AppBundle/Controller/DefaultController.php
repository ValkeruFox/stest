<?php

namespace AppBundle\Controller;

use AppBundle\Service\ProductService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class DefaultController extends Controller
{
    private const JSON_PARSE_ERROR_MESSAGE = 'Unable to parse input data';
    private $productService;

    public function __construct(ProductService $service)
    {
        $this->productService = $service;
    }

    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request): Response
    {
        $this->getDoctrine()->getManager();

        // replace this example code with whatever you need
        return $this->render('default/index.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.project_dir')) . DIRECTORY_SEPARATOR,
        ]);
    }

    /**
     * @param Request $request
     *
     * @return Response
     *
     * @route("/find")
     */
    public function getIdAction(Request $request): Response
    {
        $postdata = $request->request->get('content');
        $name     = json_decode($postdata, true)['name'];

        if (!$name) {
            return $this->buildResponse(json_encode([
                'success' => false,
                'message' => self::JSON_PARSE_ERROR_MESSAGE
            ]));
        }

        $result = $this->productService->findProductByName($name);

        return $this->buildResponse(json_encode($result));
    }

    /**
     * @param Request $request
     *
     * @return Response
     *
     * @route("/list")
     */
    public function listAction(Request $request): Response
    {
        $result = $this->productService->findAll();

        return $this->buildResponse(json_encode($result));
    }

    /**
     * @param Request $request
     *
     * @return Response
     * @throws \Doctrine\DBAL\ConnectionException
     *
     * @route("/new")
     */
    public function createAction(Request $request): Response
    {
        $postdata = $request->request->get('content');

        $productInfo = json_decode($postdata, true);
        if (!$productInfo) {
            $result = [
                'success' => false,
                'message' => self::JSON_PARSE_ERROR_MESSAGE,
            ];

            return $this->buildResponse(json_encode($result));
        }

        $result = $this->productService->createProduct($productInfo);

        return $this->buildResponse(json_encode($result));
    }

    /**
     * @param Request $request
     *
     * @return Response
     *
     * @route("/delete")
     */
    public function deleteAction(Request $request): Response
    {
        $postdata = $request->request->get('content');

        $productId = json_decode($postdata, true)['id'];

        if (!$productId) {
            $result = [
                'success' => false,
                'message' => self::JSON_PARSE_ERROR_MESSAGE,
            ];

            return $this->buildResponse(json_encode($result));
        }

        $result = $this->productService->deleteProduct($productId);

        return $this->buildResponse(json_encode($result));
    }

    private function buildResponse(string $str): Response
    {
        $resp = new Response($str);
        $resp->headers->set('Content-type', 'application/json');

        return $resp;
    }
}
