<?php
/**
 * Created by PhpStorm.
 * User: valkeru
 * Date: 04.04.18
 * Time: 13:23
 */

namespace AppBundle\Service;

use AppBundle\Entity\Product;

class ProductService
{
    private $em;

    public function __construct(\Doctrine\ORM\EntityManagerInterface $entityManager)
    {
        $this->em = $entityManager;
    }

    /**
     * @param array $productInfo
     *
     * @return array
     * @throws \Doctrine\DBAL\ConnectionException
     */
    public function createProduct(array $productInfo): array
    {
        $this->em->beginTransaction();

        try {
            $product = new Product();
            $product->setName($productInfo['name']);
            $this->em->persist($product);
            $this->em->flush();
            $this->em->commit();
        } catch (\Exception $e) {
            $this->em->rollBack();

            return [
                'success' => false,
                'message' => 'Failed to save product',
            ];
        }

        return [
            'success' => true,
            'message' => "Product {$product->getName()} succesfully added with id {$product->getId()}",
        ];
    }

    /**
     * @param string $name
     *
     * @return array
     */
    public function findProductByName(string $name): array
    {
        /** @var Product $product */
        $product = $this->em->getRepository(Product::class)->findOneBy(['name' => $name]);

        if ($product === NULL) {
            return [
                'success' => false,
                'message' => "Product {$name} not found"
            ];
        }

        return [
            'success'    => true,
            'product_id' => $product->getId()
        ];
    }

    /**
     * @return array
     */
    public function findAll(): array
    {
        /** @var Product[] $products */
        $products = $this->em->getRepository(Product::class)->findAll();

        if (empty($products)) {
            return [
                'success' => false,
                'message' => "Products not found"
            ];
        }

        $data = [];

        foreach ($products as $product) {
            $data[] = [
                'id'   => $product->getId(),
                'name' => $product->getName()
            ];
        }

        return [
            'success'      => true,
            'product_list' => json_encode($data),
        ];
    }

    /**
     * @param int $id
     *
     * @return array
     */
    public function deleteProduct(int $id): array
    {
        $product = $this->em->getRepository(Product::class)->findOneBy(['id' => $id]);

        if ($product === NULL) {
            return [
                'success' => false,
                'message' => "Product with id {$id} not found"
            ];
        }

        $this->em->beginTransaction();

        try {
            $this->em->remove($product);
            $this->em->flush();
            $this->em->commit();
        } catch (\Exception $e) {
            $this->em->rollBack();

            return [
                'success' => false,
                'message' => "Failed to remove product with id {$id}"
            ];
        }

        return [
            'success' => true,
            'message' => "Product with id {$id} successfully removed"
        ];
    }
}
