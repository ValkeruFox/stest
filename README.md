Запрос отправляется метолом POST, содержит поле `content` с json следующего содержания:  
* Для метода `add`  
    +   `{"name": "prodname"}`, где prodname - имя добавляемого элемента  
* Для метода `find`  
    +   `{"name": "prodname"}`, где prodname - имя искомого элемента  
* Для метода `list`  
    +   GET или POST-запрос, не содержищий тела  
* Для метода `delete`  
    +   `{"id": "prodid"}`, где prodid - ID удаляемого элемента

Ответ содержит статус выполнения (success, значения true либо false) и сообщение с результатом запроса или описанием ошибки  
Пример запроса:
```bash
curl -X POST http://stest.local/find -d 'content={"name": "testprod1"}'
```
Ответ:
`{"success":true,"product_id":1}`
